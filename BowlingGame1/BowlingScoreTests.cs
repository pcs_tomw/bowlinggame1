﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace BowlingGame1
{
    [TestFixture]
    class BowlingScoreTests
    {

        [Test]
        public void Creation()
        {
            var game = new Game();
            Assert.That(game, Is.Not.Null);
        }

        [Test]
        public void AddFrame()
        {
            var game = new Game();
            game.Frames.Add(new Frame());
            Assert.That(game.Frames.Count, Is.EqualTo(1));
        }

        [Test]
        public void FirstRollAddsFrame()
        {
            var game = new Game();
            game.Roll(0);
            Assert.That(game.Frames.Count, Is.EqualTo(1));
        }

        [Test]
        public void FirstRollAddsBall()
        {
            var game = new Game();
            game.Roll(0);
            Assert.That(game.Frames[0].Balls.Count, Is.EqualTo(1));
        }

        [Test]
        public void FirstRollPinsIsSaved()
        {
            var game1 = new Game();
            game1.Roll(5);
            Assert.That(game1.Frames[0].Balls[0].Pins, Is.EqualTo(5));
            var game2 = new Game();
            game2.Roll(3);
            Assert.That(game2.Frames[0].Balls[0].Pins, Is.EqualTo(3));
        }

        [Test]
        public void RollOfEleven()
        {
            var game = new Game();
            Assert.Throws<ArgumentOutOfRangeException>(() => game.Roll(11));
        }


        //[Test]
        //public void FirstRollIncrementsCurrentFrame()
        //{
        //    var game = new Game();
        //    game.Roll(0);
        //    Assert.That(game.Frames.Count, Is.EqualTo(1));
        //}

        [Test]
        public void EmptyGameHasNoScore()
        {
            var game = new Game();

            Assert.That(game.Score, Throws.Exception);
        }



        //[Test]
        //public void RollValueIsStored()
        //{
        //    var game = new Game();
        //    game.Roll(5);
        //    Assert.That(game.Frames[0].Balls[0].Pins, Is.EqualTo(5));
        //}


    }
}
