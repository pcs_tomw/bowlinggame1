﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BowlingGame1
{
    public class Game
    {
        private bool gameComplete;
        public List<Frame> Frames;

        public Game()
        {
            Frames = new List<Frame>();
            gameComplete = false;
            
        }

        public void Roll(int pins)
        {
           Frames.Add(new Frame());
            Frames[0].Balls.Add(new Ball(pins));
        }

        public int Score()
        {
            if (gameComplete == false)
            {
                throw new Exception("Game Not Complete");
            }
            return 0;
        }
    }

    public class Frame
    {
        public List<Ball> Balls;

        public Frame()
        {
            Balls = new List<Ball>();
        }
    }

    public class Ball
    {
       
        public int Pins { get; }
        public Ball(int pins)
        {
            if (pins>10) throw new ArgumentOutOfRangeException();
            Pins = pins;
        }

    }
}
